import * as fs from "fs";
const idleTimer = require("@paulcbetts/system-idle-time");
const ps = require("ps-node");

const CONFIG_PATH = "conf/idle_watchdog.json";
const defaultConf = {
  apps: [""],
  idleTimeoutMs: 10 * 60 * 1000,
  pollIntervalMs: 30 * 1000,
};

let idleState = false;
const config = readConfigSync();
initLoop();

function readConfigSync() {
  let userConf = {};
  try {
    const confFile = fs.readFileSync(CONFIG_PATH, "utf8");
    userConf = JSON.parse(confFile);
  } catch {
    console.log("Can't load " + CONFIG_PATH);
    process.exit(1);
  }
  return Object.assign({}, defaultConf, userConf);
}

function initLoop(): void {
  setInterval(() => {
    checkUserIdleness();
  }, config.pollIntervalMs);
}

function checkUserIdleness(): void {
  if (isEnteringIdleState()) {
    killAppsByName(config.apps);
  }
  idleState = isUserIdle();
}

function isEnteringIdleState(): boolean {
  return !idleState && isUserIdle();
}

function isUserIdle(): boolean {
  const idleTimeMs: number = idleTimer.getIdleTime();
  return idleTimeMs > config.idleTimeoutMs;
}

function killAppsByName(names: string[]): void {
  names.forEach(killAppNamed);
}

function killAppNamed(name: string): void {
  const query = {
    command: name,
  };
  ps.lookup(query, killAllProcs);
}

function killAllProcs(err: any, resultList: any[]): void {
  if (err) {
    return;
  }
  resultList.map((x) => x.pid).forEach(killAppById);
}

function killAppById(id: string): void {
  ps.kill(id);
}
