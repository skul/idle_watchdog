# Idle Watchdog
The idle watchdog service kills processes when the user is inactive. It is a simple node application written in typescript.

## Windows Installation
This project uses native modules that needs to be compiled. If you do not already have node-gyp and its dependencies, these should  be installed first
```
npm install --global --production windows-build-tools
npm install --global node-gyp
setx PYTHON "%USERPROFILE%\.windows-build-tools\python27\python.exe"
```
Once node-gyp is installed, restart your shell.

Install dependencies the usual way 
```
npm install
```

Template the default configs by copying `conf-sample` as a new `conf` directory.

Finally, start the application
```
npm start
```

## Configurations
Idle watchdog needs a conf file to run successfully. The config file `conf/idle_watchdog.json` should be placed with the main executable.

These are the main attributes that can be configured
| Name | Type | Presence | Default | Description |
| ---- | ---- | -------- | ------- | ----------- |
| apps | List of strings | required |  | List of processes to kill when idle. All applications which match these image names will be terminated. |
| idleTimeoutMs | int | optional | 10 minutes | User is considered idle after there has been no input for this duration. In milliseconds. |
| pollIntervalMs | int | optional | 30 seconds | Watchdog polls for inactivity at these intervals. In milliseconds. |

This is a sample config file. Using this config, the watchdog will kill all notepad instances when there has been no user input for 30s.
```json
{
  "apps": ["notepad"],
  "idleTimeoutMs": 30000,
  "pollIntervalMs": 10000
}
```

## Packaging Distributable
The watchdog can be exported as a standalone app. The app can then be configured to run in the background to monitor for user inactivity.

Pkg is used to distribute the app. Start by installing pkg module globally.
```
npm install -g pkg
```

Next, build the standalone app with
```
npm run dist
```
The executables will be built and put into the `dist/` folder. Copy the executable for your target platform together with any `.node` files. Distribute these files to your target machine together with your config file.

This app runs in the foreground with a visible window. To hide the window, run the app through this simple `.vbs` script
```
CreateObject("Wscript.Shell").Run "main-win.exe", 0
```
